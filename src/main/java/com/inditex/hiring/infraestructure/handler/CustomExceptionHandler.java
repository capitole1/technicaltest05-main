package com.inditex.hiring.infraestructure.handler;

import com.inditex.hiring.domain.exception.NoSuchResourceFoundException;
import com.inditex.hiring.domain.exception.StartEndDateOfferException;
import com.inditex.hiring.infraestructure.adapters.inbound.rest.model.ErrorDto;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.sql.Timestamp;


@ControllerAdvice
@Slf4j
public class CustomExceptionHandler {

    @ExceptionHandler(EmptyResultDataAccessException.class)
    public ResponseEntity<ErrorDto> handleDateException(EmptyResultDataAccessException ex) {
        log.error("Handling date exception: {}", ex.getMessage());
        ErrorDto errorDto = new ErrorDto(HttpStatus.NOT_FOUND.value(), new Timestamp(System.currentTimeMillis()), "No existe registro");
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorDto);
    }

    @ExceptionHandler(NoSuchResourceFoundException.class)
    public ResponseEntity<ErrorDto> handleNoSuchResourceFoundException(NoSuchResourceFoundException ex) {
        log.error("Handling resource not found exception: {}", ex.getMessage());
        ErrorDto errorDto = new ErrorDto(HttpStatus.NOT_FOUND.value(), new Timestamp(System.currentTimeMillis()), "No se encontro el recurso solicitado");
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorDto);
    }

    @ExceptionHandler(UnsupportedOperationException.class)
    public ResponseEntity<ErrorDto> handleUnsupportedOperationException(UnsupportedOperationException ex) {
        log.error("Handling resource not found exception: {}", ex.getMessage());
        ErrorDto errorDto = new ErrorDto(HttpStatus.UNAUTHORIZED.value(), new Timestamp(System.currentTimeMillis()), ex.getMessage());
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(errorDto);
    }


    @ExceptionHandler(StartEndDateOfferException.class)
    public ResponseEntity<ErrorDto> handleStartEndDateOfferException(StartEndDateOfferException ex) {
        log.error("Handling resource not found exception: {}", ex.getMessage());
        ErrorDto errorDto = new ErrorDto(HttpStatus.NOT_FOUND.value(), new Timestamp(System.currentTimeMillis()), "La fecha fin no puede ser menor a la de comienzo");
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorDto);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorDto> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        log.error("Handling resource not found exception: {}", ex.getMessage());
        ErrorDto errorDto = new ErrorDto(HttpStatus.BAD_REQUEST.value(), new Timestamp(System.currentTimeMillis()), "Error en alguno de los campos de la llamada");
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorDto);
    }



    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorDto> handleGenericException(Exception ex) {
        log.error("Handling generic not found exception: {}", ex.getMessage());
        ErrorDto errorDto = new ErrorDto(HttpStatus.INTERNAL_SERVER_ERROR.value(), new Timestamp(System.currentTimeMillis()), "Error generico");
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorDto);
    }

}
