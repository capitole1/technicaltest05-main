package com.inditex.hiring.infraestructure.adapters.outbound.persistence.mapper;

import com.inditex.hiring.domain.model.Offer;
import com.inditex.hiring.infraestructure.adapters.outbound.persistence.entity.OfferEntity;
import org.mapstruct.*;

import java.sql.Timestamp;
import java.util.List;


@Mapper(componentModel = "spring",builder = @Builder(disableBuilder = true))

public interface OfferMapper {
    @Mapping(target = "productPartnumber", ignore = true)
    @Mapping(target = "startDate", ignore = true)
    @Mapping(target = "endDate", ignore = true)
    Offer mapperToModel(OfferEntity offerEntity);

    @AfterMapping
    default void afterMappingToModel(OfferEntity offerEntity, @MappingTarget Offer offer) {
        // productPartnumber TTMMMMQQQ:
        if(offerEntity.getModel()!=null && offerEntity.getModel()!=null && offerEntity.getQuality()!=null)
            offer.setProductPartnumber(offerEntity.getSize() + offerEntity.getModel() + offerEntity.getQuality());

        offer.setStartDate(offerEntity.getStartDate().toLocalDateTime());
        if (offerEntity.getEndDate() != null)
            offer.setEndDate(offerEntity.getEndDate().toLocalDateTime());
    }



    @Mapping(target = "size", ignore = true)
    @Mapping(target = "model", ignore = true)
    @Mapping(target = "quality", ignore = true)
    @Mapping(target = "startDate", ignore = true)
    @Mapping(target = "endDate", ignore = true)
    public abstract OfferEntity mapperToEntity(Offer offer);


    @AfterMapping
    default void afterMappingToEntity(@MappingTarget OfferEntity offerEntity, @org.jetbrains.annotations.NotNull Offer offer) {
        // productPartnumber TTMMMMQQQ:
        if (offer.getProductPartnumber() != null && offer.getProductPartnumber().length() == 9) {
            offerEntity.setSize(offer.getProductPartnumber().substring(0, 2));
            offerEntity.setModel(offer.getProductPartnumber().substring(2, 6));
            offerEntity.setQuality(offer.getProductPartnumber().substring(6));
        }
        offerEntity.setStartDate(Timestamp.valueOf(offer.getStartDate()));
        if (offer.getEndDate() != null)
            offerEntity.setEndDate(Timestamp.valueOf(offer.getEndDate()));

    }

    public abstract List<Offer> mapperToModel(List<OfferEntity> offerEntityList);
}
