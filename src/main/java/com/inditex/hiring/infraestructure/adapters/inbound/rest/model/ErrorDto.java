package com.inditex.hiring.infraestructure.adapters.inbound.rest.model;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;

@NoArgsConstructor
@Builder
@Setter
@Getter
public class ErrorDto {

    private Integer codigo;
    private Timestamp timestamp;
    private String detail;

    public ErrorDto(Integer codigo, Timestamp timestamp, String detail) {
        this.codigo = codigo;
        this.timestamp = timestamp;
        this.detail = detail;
    }
}

