package com.inditex.hiring.infraestructure.adapters.inbound.rest.model;

import com.inditex.hiring.application.validation.LocalDateTimeFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;


/**
 * Use this POJO for offer service end point responses.
 */
@Getter
@Setter
@Builder
public class OfferDto implements Serializable {

    private static final long serialVersionUID = 448171649369562796L;

    @ApiModelProperty(value = "Offer id", name = "offerId", example = "1234", required = true)
    @NotNull(message = "Mandatory value offerId")
    private Long offerId;

    @ApiModelProperty(value = "Brand id", name = "brandId", example = "1234", required = true)
    @NotNull(message = "Mandatory value brandId")
    private Integer brandId;

    @ApiModelProperty(value = "Start Date", name = "startDate", example = "yyyy-MM-dd HH:mm", required = true)
    @NotEmpty(message = "No empty value startDate")
    @LocalDateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private String startDate;

    @ApiModelProperty(value = "Start Date", name = "endDate", example = "yyyy-MM-dd HH:mm")
    @LocalDateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private String endDate;

    @ApiModelProperty(value = "Id List Price", name = "priceListId", example = "1")
    private Long priceListId;

    @ApiModelProperty(value = "Product Number", name = "productPartnumber", example = "001112222")
    private String productPartnumber;

    @ApiModelProperty(value = "Priority number", name = "priority", example = "1")
    private Integer priority;

    @ApiModelProperty(value = "Price", name = "price", example = "122")
    private BigDecimal price;

    @ApiModelProperty(value = "Current ISO", name = "currencyIso", example = "USD")
    private String currencyIso;

    public OfferDto(){

    }

    public OfferDto(Long offerId, Integer brandId, String startDate, String endDate, Long priceListId, String productPartnumber, Integer priority, BigDecimal price, String currencyIso) {
        this.offerId = offerId;
        this.brandId = brandId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.priceListId = priceListId;
        this.productPartnumber = productPartnumber;
        this.priority = priority;
        this.price = price;
        this.currencyIso = currencyIso;
    }
}