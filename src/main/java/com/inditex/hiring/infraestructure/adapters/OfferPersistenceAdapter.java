package com.inditex.hiring.infraestructure.adapters;

import com.inditex.hiring.application.ports.outbound.OfferOutputPort;
import com.inditex.hiring.domain.model.Offer;
import com.inditex.hiring.infraestructure.adapters.outbound.persistence.entity.OfferEntity;
import com.inditex.hiring.infraestructure.adapters.outbound.persistence.mapper.OfferMapper;
import com.inditex.hiring.infraestructure.adapters.outbound.persistence.repository.OfferRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class OfferPersistenceAdapter implements OfferOutputPort {

    private final OfferRepository offerRepository;

    private final OfferMapper offerMapper;

    public OfferPersistenceAdapter(OfferRepository offerRepository, OfferMapper offerMapper) {
        this.offerRepository = offerRepository;
        this.offerMapper = offerMapper;
    }

    @Override
    public Offer create(Offer offer) {
        if (offerRepository.existsById(offer.getOfferId())) {
            throw new UnsupportedOperationException("Exist Offer with this id. Offer PUT endpoint is not in scope exam " + offer.getProductPartnumber() + " already exists");
        }

        OfferEntity offerEntityCreated = offerRepository.save(offerMapper.mapperToEntity(offer));
        return offerMapper.mapperToModel(offerEntityCreated);
    }


    @Override
    public boolean deleteById(Long id) {
        offerRepository.deleteById(id);
        return true;
    }

    @Override
    public Optional<Offer> findById(Long id) {
        Optional<OfferEntity> offerEntityOptional = offerRepository.findById(id);

        if (offerEntityOptional.isEmpty()) {
            return Optional.empty();
        }
        Offer offer = offerMapper.mapperToModel(offerEntityOptional.get());
        return Optional.of(offer);
    }


    @Override
    public void deleteAll() {
        offerRepository.deleteAll();
    }

    @Override
    public List<Offer> listOffers() {
        List<OfferEntity> offerEntityList = offerRepository.findAll();
        return offerMapper.mapperToModel(offerEntityList);
    }
}
