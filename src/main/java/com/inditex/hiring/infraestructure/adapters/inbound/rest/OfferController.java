package com.inditex.hiring.infraestructure.adapters.inbound.rest;


import com.inditex.hiring.application.mapper.OfferInboundMapper;
import com.inditex.hiring.application.ports.inbound.*;
import com.inditex.hiring.domain.model.Offer;
import com.inditex.hiring.infraestructure.adapters.inbound.rest.model.OfferDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/offer")
@Api(value = "Offer Controller", tags = {"Offer Controller"})
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PATCH, RequestMethod.PUT, RequestMethod.DELETE})
public class OfferController {

    private final CreateOfferUseCase createOfferUseCase;
    private final FindOfferUseCase findOfferUseCase;
    private final DeleteOfferUseCase deleteOfferUseCase;
    private final DeleteAllOffersUserCase deleteAllOffersUserCase;
    private final FindAllOfersUseCase findAllOfersUseCase;
    private final OfferInboundMapper mapper;

    public OfferController(CreateOfferUseCase createOfferUseCase, FindOfferUseCase findOfferUseCase, DeleteOfferUseCase deleteOfferUseCase, DeleteAllOffersUserCase deleteAllOffersUserCase, FindAllOfersUseCase findAllOfersUseCase, OfferInboundMapper mapper) {
        this.createOfferUseCase = createOfferUseCase;
        this.findOfferUseCase = findOfferUseCase;
        this.deleteOfferUseCase = deleteOfferUseCase;
        this.deleteAllOffersUserCase = deleteAllOffersUserCase;
        this.findAllOfersUseCase = findAllOfersUseCase;
        this.mapper = mapper;
    }

    @ApiOperation(value = "Crea una oferta")
    @PostMapping()
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully create an Offer"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    public ResponseEntity<OfferDto> createOffer(@RequestBody @Valid OfferDto offerdto) {
        Offer offer = mapper.mapperToModel(offerdto);
        Offer offerSaved = createOfferUseCase.create(offer);

        URI location = UriComponentsBuilder.fromUriString("http://localhost:8080")
                .path("/offers/{offerId}")
                .buildAndExpand(offerSaved.getOfferId())
                .toUri();
         return ResponseEntity.created(location).body(mapper.mappToDto(offerSaved));
    }


    @ApiOperation(value = "/Borrar por id")
    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> deleteOfferById(@PathVariable("id") Long id) {
        if (!deleteOfferUseCase.deleteById(id))
            return ResponseEntity.notFound().build();
        return ResponseEntity.noContent().build();
    }

    @ApiOperation(value = "Obtener por id")
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OfferDto> getOfferById(@PathVariable Long id) {
        return ResponseEntity.ok(mapper.mappToDto(findOfferUseCase.findById(id)));
    }

    @ApiOperation(value = "Eliminar todas las ofertas")
    @DeleteMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> deleteAllOffers() {
        deleteAllOffersUserCase.deleteAll();
        return ResponseEntity.noContent().build();

    }

    @ApiOperation(value = "Endopint para optener todas las ofertas")
    @GetMapping()
    public ResponseEntity<List<OfferDto>> getAllOffers() {
        //solo para mostrar los manejos de stream  y funcional lo podia hacer mapeando la lista a lista
        List<OfferDto> offerDtos = findAllOfersUseCase.findAll().stream().map(mapper::mappToDto).collect(Collectors.toList());
        return ResponseEntity.ok(offerDtos);
    }


}