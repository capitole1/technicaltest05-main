package com.inditex.hiring.infraestructure.adapters.outbound.persistence.repository;


import com.inditex.hiring.infraestructure.adapters.outbound.persistence.entity.OfferEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Interface Offer entity for create update delete offers
 */

@Repository
public interface OfferRepository extends JpaRepository<OfferEntity, Long> {
}
