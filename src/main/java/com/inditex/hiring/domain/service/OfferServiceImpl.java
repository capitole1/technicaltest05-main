package com.inditex.hiring.domain.service;

import com.inditex.hiring.application.ports.inbound.*;
import com.inditex.hiring.application.ports.outbound.OfferOutputPort;
import com.inditex.hiring.domain.exception.NoSuchResourceFoundException;
import com.inditex.hiring.domain.model.Offer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * OfferService class
 */
@Service
public class OfferServiceImpl implements CreateOfferUseCase, DeleteOfferUseCase, FindOfferUseCase, DeleteAllOffersUserCase, FindAllOfersUseCase {

    private static final Logger LOGGER = LoggerFactory.getLogger(OfferServiceImpl.class);
    private final OfferOutputPort offerOutputPort;

    public OfferServiceImpl(OfferOutputPort offerOutputPort) {
        this.offerOutputPort = offerOutputPort;
    }

    @Override
    public Offer create(Offer offer) {
        LOGGER.debug("OfferService - create");
        return offerOutputPort.create(offer);
    }

    @Override
    public void deleteAll() {
        LOGGER.debug("OfferService - deleteAll");
        offerOutputPort.deleteAll();
    }


    @Override
    public Offer findById(Long id) {
        LOGGER.debug("OfferService - findById id {} : ", id);
        return offerOutputPort.findById(id)
                .orElseThrow(() -> new NoSuchResourceFoundException("No se encontro la oferta con ID: " + id));
    }

    @Override
    public boolean deleteById(Long id) {
        LOGGER.debug("OfferService - delete by id {} : ", id);
        if (this.findById(id) != null) {
            offerOutputPort.deleteById(id);
            return true;
        }
        return false;
    }

    @Override
    public List<Offer> findAll() {
        LOGGER.debug("OfferService -findAll ");
        return offerOutputPort.listOffers();
    }
}
