package com.inditex.hiring.domain.model;


import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Builder

@ToString
@Getter
@Setter
public class Offer {

    private Long offerId;

    private Integer brandId;

    private LocalDateTime startDate;

    private LocalDateTime endDate;

    private Integer priceListId;

    private String productPartnumber;

    private Integer priority;

    private BigDecimal price;

    private String currencyIso;

    public Offer(){

    }

    public Offer(Long offerId, Integer brandId, LocalDateTime startDate, LocalDateTime endDate, Integer priceListId, String productPartnumber, Integer priority, BigDecimal price, String currencyIso) {
        this.offerId = offerId;
        this.brandId = brandId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.priceListId = priceListId;
        this.productPartnumber = productPartnumber;
        this.priority = priority;
        this.price = price;
        this.currencyIso = currencyIso;
    }
}
