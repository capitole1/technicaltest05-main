package com.inditex.hiring.domain.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class StartEndDateOfferException extends RuntimeException {

    private static final long serialVersionUID = -8749643454264131447L;

    public StartEndDateOfferException(String msg) {
        super(msg);
    }
}
