package com.inditex.hiring.application.ports.inbound;

/**
 * Caso de uso para borrar una foerta en particular
 */
public interface DeleteOfferUseCase {

    boolean deleteById(Long offer);
}
