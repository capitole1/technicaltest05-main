package com.inditex.hiring.application.ports.inbound;

/**
 * Caso de uso para eliminar todas las ofertas
 */
public interface DeleteAllOffersUserCase {

    void deleteAll();
}
