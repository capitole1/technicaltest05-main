package com.inditex.hiring.application.ports.inbound;


import com.inditex.hiring.domain.model.Offer;

/**
 * Caso de uso para buscar por id
 */
public interface FindOfferUseCase {

    Offer findById(Long id);
}
