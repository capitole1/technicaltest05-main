package com.inditex.hiring.application.ports.inbound;

import com.inditex.hiring.domain.model.Offer;

import java.util.List;

public interface FindAllOfersUseCase {

    List<Offer> findAll();
}
