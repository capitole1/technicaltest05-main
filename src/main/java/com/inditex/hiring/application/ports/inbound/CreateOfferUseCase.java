package com.inditex.hiring.application.ports.inbound;

import com.inditex.hiring.domain.model.Offer;

/**
 * Caso de uso para crear una oferta
 */
public interface CreateOfferUseCase {

    Offer create(Offer offer);
}
