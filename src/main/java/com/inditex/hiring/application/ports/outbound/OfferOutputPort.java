package com.inditex.hiring.application.ports.outbound;

import com.inditex.hiring.domain.model.Offer;

import java.util.List;
import java.util.Optional;

/**
 * Interfaz de salida de la capa de aplicacion
 */

public interface OfferOutputPort {
    Offer create(Offer offer);

    boolean deleteById(Long id);

    Optional<Offer> findById(Long id);

    void deleteAll();

    List<Offer> listOffers();
}
