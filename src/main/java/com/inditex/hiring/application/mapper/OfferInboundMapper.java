package com.inditex.hiring.application.mapper;

import com.inditex.hiring.domain.exception.StartEndDateOfferException;
import com.inditex.hiring.domain.model.Offer;
import com.inditex.hiring.infraestructure.adapters.inbound.rest.model.OfferDto;
import org.mapstruct.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.FIELD,builder = @Builder(disableBuilder = true))
public interface OfferInboundMapper {

    @Mapping(target = "startDate", ignore = true)
    @Mapping(target = "endDate", ignore = true)
    Offer mapperToModel(OfferDto offerDto);

    @Mapping(target = "startDate", ignore = true)
    @Mapping(target = "endDate", ignore = true)
    OfferDto mappToDto(Offer offer);

    @AfterMapping
    default void afterMapperToDto(@MappingTarget OfferDto offerDto, Offer offer) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        if (offer.getStartDate() != null){
            offerDto.setStartDate(offer.getStartDate().format(formatter));
        }
        if (offer.getEndDate() != null){
            offerDto.setEndDate(offer.getEndDate().format(formatter));
        }


    }

    @AfterMapping
    default void afterMapperToModel(@MappingTarget Offer offer, OfferDto offerDto) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        //startDate nunca puede ser null por negocio
        LocalDateTime startDate = parseDateTime(offerDto.getStartDate(), formatter);

        // Si son distinto de null entonces valido desde hasta
        if (offerDto.getEndDate() != null){
            LocalDateTime endDate = parseDateTime(offerDto.getEndDate(), formatter);
            validateDateEndAfterStart(startDate, endDate);
            offer.setStartDate(startDate);
            offer.setEndDate(endDate);
        }
    }

    private LocalDateTime parseDateTime(String dateTimeStr, DateTimeFormatter formatter) {
        return LocalDateTime.parse(dateTimeStr, formatter);
    }

    private void validateDateEndAfterStart(LocalDateTime startDate, LocalDateTime endDate) {
        if (startDate.isAfter(endDate)) {
            throw new StartEndDateOfferException("EndDate must be after startDate.");
        }
    }

}
