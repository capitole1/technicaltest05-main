package com.inditex.hiring.domain.service;

import com.inditex.hiring.application.ports.outbound.OfferOutputPort;
import com.inditex.hiring.domain.exception.NoSuchResourceFoundException;
import com.inditex.hiring.domain.model.Offer;
import com.inditex.hiring.util.MockUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class OfferServiceImplTest {

    @Mock
    private OfferOutputPort offerOutputPort;

    private OfferServiceImpl offerService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        offerService = new OfferServiceImpl(offerOutputPort);
    }

    @Test
    void testCreateOffer() {
        Offer offer = MockUtil.getOffer1();

        when(offerOutputPort.create(offer)).thenReturn(offer);

        Offer createdOffer = offerService.create(offer);

        assertNotNull(createdOffer);
        assertEquals(offer, createdOffer);
        verify(offerOutputPort, times(1)).create(offer);
    }

    @Test
    void testDeleteAllOffers() {
        offerService.deleteAll();

        verify(offerOutputPort, times(1)).deleteAll();
    }

    @Test
     void testFindOfferById() {
        Long offerId = 1L;
        Offer offer = MockUtil.getOffer1();
        when(offerOutputPort.findById(offerId)).thenReturn(Optional.of(offer));

        Offer foundOffer = offerService.findById(offerId);

        assertNotNull(foundOffer);
        assertEquals(offer, foundOffer);
        verify(offerOutputPort, times(1)).findById(offerId);
    }

    @Test
     void testFindOfferByIdNotFound() {
        Long offerId = 1L;
        when(offerOutputPort.findById(offerId)).thenReturn(Optional.empty());

        NoSuchResourceFoundException exception = assertThrows(
                NoSuchResourceFoundException.class,
                () -> offerService.findById(offerId)
        );

        assertEquals("No se encontro la oferta con ID: " + offerId, exception.getMessage());
        verify(offerOutputPort, times(1)).findById(offerId);
    }

    @Test
     void testDeleteOfferById() {
        Long offerId = 1L;
        Offer offer = MockUtil.getOffer1();
        when(offerOutputPort.findById(offerId)).thenReturn(Optional.of(offer));

        boolean result = offerService.deleteById(offerId);

        assertTrue(result);
        verify(offerOutputPort, times(1)).findById(offerId);
        verify(offerOutputPort, times(1)).deleteById(offerId);
    }

    @Test
    void testDeleteOfferByIdNotFound() {
        Long offerId = 1L;
        when(offerOutputPort.findById(offerId)).thenReturn(Optional.empty());

        //Chequeamos que venga la exception que esperamos
        assertThrows(NoSuchResourceFoundException.class, () -> {
            offerService.deleteById(offerId);
        });

        verify(offerOutputPort, times(1)).findById(offerId);
        verify(offerOutputPort, never()).deleteById(anyLong());
    }

    @Test
    void testFindAllOffers() {
        List<Offer> offerList = MockUtil.listOffers();
        when(offerOutputPort.listOffers()).thenReturn(offerList);

        List<Offer> foundOffers = offerService.findAll();

        assertNotNull(foundOffers);
        assertEquals(offerList, foundOffers);
        verify(offerOutputPort, times(1)).listOffers();
    }
}
