package com.inditex.hiring.domain.service;

import com.inditex.hiring.application.ports.outbound.OfferOutputPort;
import com.inditex.hiring.domain.exception.NoSuchResourceFoundException;
import com.inditex.hiring.domain.model.Offer;
import com.inditex.hiring.util.MockUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.util.AssertionErrors.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.yml")
@Transactional
public class OfferServiceImplIntegrationTest {

    @Autowired
    private OfferOutputPort offerOutputPort;

    @Autowired
    private OfferServiceImpl offerService;

    @Test
    public void testCreateOffer() {
        Offer offer = MockUtil.getOffer1();

        Offer createdOffer = offerService.create(offer);
        assertNotNull(createdOffer.getOfferId());
        assertEquals("Se creo con el id correcto:", offer.getOfferId(), createdOffer.getOfferId());
        assertEquals("ProductPartNumber",offer.getProductPartnumber(), createdOffer.getProductPartnumber());
    }

    @Test
    public void testFindAllOffers() {
        Offer offer1 = MockUtil.getOffer1();
        Offer offer2 = MockUtil.getOffer2();

        offerOutputPort.create(offer1);
        offerOutputPort.create(offer2);

        // Obtener todas las ofertas
        List<Offer> foundOffers = offerService.findAll();

        assertNotNull(foundOffers);
        assertEquals("Se encontraron ",2, foundOffers.size());
    }

    @Test
    public void testDeleteById() {
        Offer offer1 = MockUtil.getOffer1();

        Offer offer2 = MockUtil.getOffer2();

        offerOutputPort.create(offer1);
        offerOutputPort.create(offer2);

        // Obtener todas las ofertas
        List<Offer> foundOffers = offerService.findAll();

        Long idToDelete = foundOffers.get(0).getOfferId();
        //Borramos
        offerService.deleteById(idToDelete);

        foundOffers = offerService.findAll();

        assertNotNull(foundOffers);
        assertEquals("Ahora solo queda 1",1, foundOffers.size());
        assertTrue(!idToDelete.equals(foundOffers.get(0).getOfferId()));

    }

    @Test
    public void testDeleteAll() {
        Offer offer1 = MockUtil.getOffer1();
        Offer offer2 = MockUtil.getOffer2();

        offerOutputPort.create(offer1);
        offerOutputPort.create(offer2);

        // Obtener todas las ofertas
        List<Offer> foundOffersPrevius = offerService.findAll();

        //Borramos
        offerService.deleteAll();

        List<Offer> foundOffers = offerService.findAll();

        assertNotNull(foundOffers);
        assertEquals("Cantidad Anterior a borrar",2, foundOffersPrevius.size());
        assertEquals("Cantidad Despues de borrar",0, foundOffers.size());
    }

    @Test
    public void testDeletebyId_when_idNotFoundl() {
        // Borramos todos los datos
        offerService.deleteAll();

        assertThrows(
                NoSuchResourceFoundException.class,
                () -> offerService.deleteById(10L)
        );
    }
}