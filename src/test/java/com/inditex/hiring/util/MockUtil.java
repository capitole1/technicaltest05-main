package com.inditex.hiring.util;

import com.inditex.hiring.domain.model.Offer;
import com.inditex.hiring.infraestructure.adapters.inbound.rest.model.OfferDto;
import com.inditex.hiring.infraestructure.adapters.outbound.persistence.entity.OfferEntity;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

public class MockUtil {

    public static OfferEntity getOfferEntity1() {
        OfferEntity offerEntity = OfferEntity.builder()
                .offerId(1L)
                .startDate(Timestamp.valueOf(LocalDateTime.now()))
                .size("00")
                .model("100")
                .quality("233").build();
        return offerEntity;
    }

    public static OfferEntity getOfferEntity2() {
        OfferEntity offerEntity = OfferEntity.builder()
                .offerId(2L)
                .startDate(Timestamp.valueOf(LocalDateTime.now()))
                .size("11")
                .model("222")
                .quality("333").build();
        return offerEntity;
    }

    public static Offer getOffer1() {
        Offer offer = Offer.builder()
                .offerId(1L)
        .startDate(LocalDateTime.now())
        .productPartnumber("000100233").build();
        return offer;
    }

    public static OfferDto getOffer1Dto() {
        OfferDto offer = OfferDto.builder()
        .offerId(1L).startDate("2020-01-01 00:00")
                .productPartnumber("000100233")
        .brandId(1).build();
        return offer;
    }

    public static OfferDto getOffer1BadRequest() {
        OfferDto offer = OfferDto.builder()
                .offerId(1L)
        .productPartnumber("00010").build();
        return offer;
    }



    public static Offer getOffer2() {
        Offer offer = Offer.builder()
                .offerId(2L)
        .startDate(LocalDateTime.now()).productPartnumber("000100299")
                .build();
          return offer;
    }

    public static OfferDto getOffertDto2() {
        OfferDto offerDto = OfferDto.builder()
                .offerId(2L)
        .startDate("2021-01-01 00:00")
        .productPartnumber("000101299").build();
        return offerDto;
    }
    public static List<OfferEntity> listOfferEntities() {
        return Arrays.asList(getOfferEntity1(), getOfferEntity2());
    }

    public static List<OfferDto> listOfferDto() {
        return Arrays.asList(getOffer1Dto(), getOffertDto2());
    }

    public static List<Offer> listOffers() {
        return Arrays.asList(getOffer1(), getOffer2());
    }
}
