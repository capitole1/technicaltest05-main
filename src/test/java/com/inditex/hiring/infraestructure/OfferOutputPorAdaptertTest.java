package com.inditex.hiring.infraestructure;

import com.inditex.hiring.domain.model.Offer;
import com.inditex.hiring.infraestructure.adapters.OfferPersistenceAdapter;
import com.inditex.hiring.infraestructure.adapters.outbound.persistence.entity.OfferEntity;
import com.inditex.hiring.infraestructure.adapters.outbound.persistence.mapper.OfferMapper;
import com.inditex.hiring.infraestructure.adapters.outbound.persistence.repository.OfferRepository;
import com.inditex.hiring.util.MockUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.EmptyResultDataAccessException;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.util.AssertionErrors.assertEquals;


@ExtendWith(MockitoExtension.class)
class OfferOutputPorAdaptertTest  {

    private OfferPersistenceAdapter offerPersistenceAdapter;
    @Mock
    private OfferRepository offerRepository;
    @Mock
    private OfferMapper offerMapper;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.offerPersistenceAdapter = new OfferPersistenceAdapter(offerRepository, offerMapper);

    }

    @Test
     void createOffer() {

        //creamos mocks
        Offer offer = MockUtil.getOffer1();

        OfferEntity offerEntity = MockUtil.getOfferEntity1();

        //mockeamos llamadas
        when(offerMapper.mapperToModel(any(OfferEntity.class))).thenReturn(offer);
        when(offerMapper.mapperToEntity(any(Offer.class))).thenReturn(offerEntity);
        when(offerRepository.save(any())).thenReturn(offerEntity);

        // llamamos al servicio a testear
        Offer savedOffer = offerPersistenceAdapter.create(offer);

        // validamos
        assertNotNull(savedOffer);
        verify(offerRepository, times(1)).save(any());
    }


    @Test
     void deleteAll() {

        // Llamamos al método que queremos probar
        offerPersistenceAdapter.deleteAll();

        // verificamos que se llama al repo
        verify(offerRepository, times(1)).deleteAll();
    }

    @Test
     void findById_when_offerFound() {
        Long offerId = 1L;
        when(offerRepository.findById(offerId)).thenReturn(Optional.of(MockUtil.getOfferEntity1()));
        when(offerMapper.mapperToModel(any(OfferEntity.class))).thenReturn(MockUtil.getOffer1());

        Optional<Offer> offerOptional = offerPersistenceAdapter.findById(offerId);

        // Verificar que se llama al método findByOfferId del repositorio
        verify(offerRepository).findById(offerId);
        //verificamos que nos devolvio oferta
        Assertions.assertTrue(offerOptional.isPresent());
    }

    @Test
     void findById_when_offerNotFound() {
        Long offerId = 1L;
        when(offerRepository.findById(offerId)).thenReturn(Optional.empty());

        Optional<Offer> offerOptional = offerPersistenceAdapter.findById(offerId);

        // Verificar que se llama al método findByOfferId del repositorio
        verify(offerRepository).findById(offerId);
        //verificamos que nos devolvio oferta
        Assertions.assertTrue(offerOptional.isEmpty());
    }

    @Test
     void deleteById_when_offerExist() {
        Long offerId = 1L;
        doNothing().when(offerRepository).deleteById(offerId);
        boolean result = offerPersistenceAdapter.deleteById(offerId);

        // Verificar que se llama al método deleteById del repositorio
        verify(offerRepository).deleteById(offerId);
        //verificamos que nos devolvio oferta
        Assertions.assertTrue(result);
    }


    @Test
     void deleteById_when_offerNotExist_ThrowException() {
        Long offerId = 10L;
        //verificamos lanza una excepcion
        doThrow(new EmptyResultDataAccessException(1)).when(offerRepository).deleteById(offerId);

        //llamamos al servicio a testesar y verficamos excepcion
        assertThrows(
                EmptyResultDataAccessException.class,
                () -> offerPersistenceAdapter.deleteById(offerId)
        );

        verify(offerRepository, times(1)).deleteById(offerId);
    }

    @Test
    void findAll() {
        // Lista mock
        List<OfferEntity> mockOffers = MockUtil.listOfferEntities();

        // Mock de la llamada al repositorio
        when(offerRepository.findAll()).thenReturn(mockOffers);
        when(offerMapper.mapperToModel(mockOffers)).thenReturn(MockUtil.listOffers());
        // Llamada al método del servicio
        List<Offer> listOffersFind = offerPersistenceAdapter.listOffers();

        // Verificación de que el método del repositorio fue llamado una vez
        verify(offerRepository, times(1)).findAll();

        // Verificación de los resultados
        assertAll(
                () -> assertEquals("Debe haber dos ofertas", 2, listOffersFind.size()),
                () -> assertEquals("El id de debe ser 1", 1L, listOffersFind.get(0).getOfferId()),
                () -> assertEquals("El id de debe ser 2", 2L, listOffersFind.get(1).getOfferId())
        );
    }
}