package com.inditex.hiring.infraestructure.adapters.outbound.persistence.mapper;

import com.inditex.hiring.domain.model.Offer;
import com.inditex.hiring.infraestructure.adapters.outbound.persistence.entity.OfferEntity;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class OfferMapperTest {

    private final OfferMapper mapper = Mappers.getMapper(OfferMapper.class);

    @Test
    public void testMapperToModel() {
        // Given
        OfferEntity offerEntity = OfferEntity.builder().offerId(1L)
        .brandId(1)
        .size("00")
        .model("0100")
        .quality("233")
        .startDate(Timestamp.valueOf(LocalDateTime.of(2023, 1, 1, 10, 0)))
        .endDate(Timestamp.valueOf(LocalDateTime.of(2023, 1, 10, 15, 0))).build();

        // When
        Offer offer = mapper.mapperToModel(offerEntity);

        // Then
        assertEquals(offerEntity.getOfferId(), offer.getOfferId());
        assertEquals(offerEntity.getBrandId(), offer.getBrandId());
        assertEquals("000100233", offer.getProductPartnumber()); // Verifica que se este parseando bien en los 3 campos pedidos
        assertEquals(LocalDateTime.of(2023, 1, 1, 10, 0), offer.getStartDate());
        assertEquals(LocalDateTime.of(2023, 1, 10, 15, 0), offer.getEndDate());
    }

    @Test
    public void testMapperToEntity() {
        // Given
        Offer offer = Offer.builder()
                .offerId(1L)
                .brandId(1)
                .productPartnumber("000100233")
        .startDate(LocalDateTime.of(2023, 1, 1, 10, 0))
        .endDate(LocalDateTime.of(2023, 1, 10, 15, 0))
                .build();
        // When
        OfferEntity offerEntity = mapper.mapperToEntity(offer);

        // Then
        assertEquals(offer.getOfferId(), offerEntity.getOfferId());
        assertEquals(offer.getBrandId(), offerEntity.getBrandId());
        assertEquals("00", offerEntity.getSize());
        assertEquals("0100", offerEntity.getModel());
        assertEquals("233", offerEntity.getQuality());
        assertEquals(Timestamp.valueOf(LocalDateTime.of(2023, 1, 1, 10, 0)), offerEntity.getStartDate());
        assertEquals(Timestamp.valueOf(LocalDateTime.of(2023, 1, 10, 15, 0)), offerEntity.getEndDate());
    }
}