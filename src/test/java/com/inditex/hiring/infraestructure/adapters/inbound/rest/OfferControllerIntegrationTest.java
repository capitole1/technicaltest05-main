package com.inditex.hiring.infraestructure.adapters.inbound.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.inditex.hiring.application.mapper.OfferInboundMapper;
import com.inditex.hiring.application.ports.inbound.*;
import com.inditex.hiring.domain.model.Offer;
import com.inditex.hiring.infraestructure.adapters.inbound.rest.model.OfferDto;
import com.inditex.hiring.util.MockUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureMockMvc
public class OfferControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CreateOfferUseCase createOfferUseCase;

    @MockBean
    private FindOfferUseCase findOfferUseCase;

    @MockBean
    private DeleteOfferUseCase deleteOfferUseCase;

    @MockBean
    private DeleteAllOffersUserCase deleteAllOffersUserCase;

    @MockBean
    private FindAllOfersUseCase findAllOfersUseCase;

    @Autowired
    private OfferInboundMapper mapper;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

    }

    @Test
    void testCreateOffer() throws Exception {
        OfferDto offerDto = MockUtil.getOffer1Dto();

        Offer offerModel = MockUtil.getOffer1();

      //  when(mapper.mapperToModel(any())).thenReturn(offerModel);
        when(createOfferUseCase.create(any())).thenReturn(offerModel);

        mockMvc.perform(MockMvcRequestBuilders.post("/offer")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(offerDto)))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.offerId").value(1));
    }

    @Test
    void testCreateOfferBadRequest() throws Exception {
        OfferDto offerDto = MockUtil.getOffer1BadRequest();


        mockMvc.perform(MockMvcRequestBuilders.post("/offer")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(offerDto)))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    void testDeleteOfferById() throws Exception {
        Long offerId = 1L;

        when(deleteOfferUseCase.deleteById(offerId)).thenReturn(true);

        mockMvc.perform(MockMvcRequestBuilders.delete("/offer/{id}", offerId))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    void testGetOfferById() throws Exception {
        Long offerId = 1L;
        Offer offerModel = MockUtil.getOffer1();

        when(findOfferUseCase.findById(offerId)).thenReturn(offerModel);

        mockMvc.perform(MockMvcRequestBuilders.get("/offer/{offerId}", offerId))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.offerId").value(1));
    }

    @Test
    void testDeleteAllOffers() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/offer"))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    void testGetAllOffers() throws Exception {


        List<Offer> offerDtoList = MockUtil.listOffers();

        when(findAllOfersUseCase.findAll()).thenReturn(offerDtoList);

        mockMvc.perform(MockMvcRequestBuilders.get("/offer"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].offerId").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].offerId").value(2));
    }
}
