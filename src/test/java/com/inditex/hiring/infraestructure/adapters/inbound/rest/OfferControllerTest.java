package com.inditex.hiring.infraestructure.adapters.inbound.rest;

import com.inditex.hiring.application.mapper.OfferInboundMapper;
import com.inditex.hiring.application.ports.inbound.*;
import com.inditex.hiring.domain.model.Offer;
import com.inditex.hiring.infraestructure.adapters.inbound.rest.model.OfferDto;
import com.inditex.hiring.util.MockUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;
import static org.springframework.test.util.AssertionErrors.assertEquals;

@ExtendWith(MockitoExtension.class)
class OfferControllerTest {

    @Mock
    private CreateOfferUseCase createOfferUseCase;

    @Mock
    private FindOfferUseCase findOfferUseCase;

    @Mock
    private DeleteOfferUseCase deleteOfferUseCase;

    @Mock
    private DeleteAllOffersUserCase deleteAllOffersUserCase;

    @Mock
    private FindAllOfersUseCase findAllOfersUseCase;

    @Mock
    private OfferInboundMapper mapper;

    @InjectMocks
    private OfferController offerController;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.offerController = new OfferController(
                createOfferUseCase, findOfferUseCase, deleteOfferUseCase, deleteAllOffersUserCase, findAllOfersUseCase, mapper);

    }

    @Test
    void testCreateOffer() {
        // Given
        OfferDto offerDto = MockUtil.getOffer1Dto();

        Offer offer = MockUtil.getOffer1();

        when(mapper.mapperToModel(offerDto)).thenReturn(offer);
        when(createOfferUseCase.create(offer)).thenReturn(offer);
        when(mapper.mappToDto(offer)).thenReturn(offerDto);
      // When
        ResponseEntity<OfferDto> response = offerController.createOffer(offerDto);

        // Then
        assertEquals("Verificamos el http status", HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals("Verificamos el id es el mismo", offer.getOfferId(), response.getBody().getOfferId());

        // verifciamos que se llamo al caso de uso
        verify(mapper).mapperToModel(offerDto);
        verify(createOfferUseCase).create(offer);
    }

    @Test
    void testDeleteOfferById() {
        // Given
        Long offerId = 1L;

        when(deleteOfferUseCase.deleteById(offerId)).thenReturn(true);

        // When
        ResponseEntity<Void> response = offerController.deleteOfferById(offerId);

        // Then
        assertEquals("verifico status http", HttpStatus.NO_CONTENT, response.getStatusCode());

        // verificamos que se llamo al caso de uso
        verify(deleteOfferUseCase).deleteById(offerId);
    }

    @Test
    void testGetOfferById() {
        // Given
        Long offerId = 1L;
        Offer offer = MockUtil.getOffer1();
        offer.setOfferId(offerId);
        OfferDto offerDto = MockUtil.getOffer1Dto();

        when(findOfferUseCase.findById(offerId)).thenReturn(offer);
        when(mapper.mappToDto(offer)).thenReturn(offerDto);

        // When
        ResponseEntity<OfferDto> response = offerController.getOfferById(offerId);

        // Then
        assertEquals("Verifico status ok", HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals("verificamos id", offerId, response.getBody().getOfferId());

        // verificamos que se llamo
        verify(findOfferUseCase).findById(offerId);
        verify(mapper).mappToDto(offer);
    }

    @Test
    void testDeleteAllOffers() {
        // When
        ResponseEntity<Void> response = offerController.deleteAllOffers();

        // Then
        assertEquals("verificamos el estatus", HttpStatus.NO_CONTENT, response.getStatusCode());

        // Verify que se llamo
        verify(deleteAllOffersUserCase).deleteAll();
    }

    @Test
    void testGetAllOffers() {
        // Given
        List<Offer> offers = MockUtil.listOffers();

        when(findAllOfersUseCase.findAll()).thenReturn(offers);
        when(mapper.mappToDto(any(Offer.class))).thenReturn(MockUtil.getOffer1Dto());

        // When
        ResponseEntity<List<OfferDto>> response = offerController.getAllOffers();

        // Then
        assertEquals("Verificamos el status", HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals("Verificamos que vengan 2", offers.size(), response.getBody().size());

        // Verify mock interactions
        verify(findAllOfersUseCase).findAll();
        verify(mapper, Mockito.times(2)).mappToDto(any(Offer.class));

    }
}